<?php 

function curl($url, $data){
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    curl_close($ch);      
    return $output;
}

function curl2($url2){
    $ch2 = curl_init(); 
    curl_setopt($ch2, CURLOPT_URL, $url2); 
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1); 
    $output2 = curl_exec($ch2); 
    curl_close($ch2);      
    return $output2;
}

$send2 = curl2("http://localhost/PHP%20API/mysql_to_json.php");

// mengubah JSON menjadi array
$data2 = json_decode($send2, TRUE);

?>

<!DOCTYPE html>
<html>
<body>

<table border="1">
<tr>
	<th> Nama Kapal </th>
	<th> Dokumen </th>
</tr>
<?php foreach($data2 as $row =>$c){ ?>
<tr>
	<?php 
	if($c["nama_kapal"]=="Kapal A"){
	?>
	<td><?php echo $c["nama_kapal"];?></td>
	<td><?php echo $c["dokumen"];?></td>
</tr>
<?php
       $data = array("nama_kapal"=>$c["nama_kapal"],"dokumen"=>$c["dokumen"]);	
       $send = curl("http://localhost/PHP%20API/get_json_post.php",json_encode($data));
      echo json_encode(array('respon'=>$send),JSON_UNESCAPED_SLASHES)."<br>";
  
	}?>

<?php 

}

?>


</table>

</body>
</html>