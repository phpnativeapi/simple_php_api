<?php 
function curl($url){
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    curl_close($ch);      
    return $output;
}

$send = curl("http://localhost/PHP%20API/mysql_to_json.php");

// mengubah JSON menjadi array
$data = json_decode($send, TRUE);

?>

<!DOCTYPE html>
<html>
<body>

<table border="1">
<tr>
	<th> Nama Kapal </th>
	<th> Dokumen </th>
</tr>
<?php foreach($data as $row =>$c){ ?>
<tr>
	<?php 
	if($c["nama_kapal"]=="Kapal A"){
	?>
	<td><?php echo $c["nama_kapal"];?></td>
	<td><?php echo $c["dokumen"];}?></td>
</tr>
<?php } ?>
</table>

</body>
</html>